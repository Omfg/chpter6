/**
 * Created by omfg on 08.11.2016.
 */
public class ShowBitsDemo {
   public void work(){
       ShowBits b = new ShowBits(8);
       ShowBits i = new ShowBits(32);
       ShowBits li = new ShowBits(64);

       System.out.println("123 В двоичном представлении: ");
       b.show(123);
       System.out.println("\n87987 в двоичном представлении: ");
       i.show(87987);
       System.out.println("\n237658768 в двоичном представлении");
       li.show(237658768);
   }
}
